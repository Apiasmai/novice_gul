import java.awt.EventQueue;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Login extends JFrame {

	private String nameTF;
	JButton btn;
	JFrame f = new JFrame();
	JTextField textField;
	 
	public Login() {

		
		super("Login");
	
		f.setVisible(true);
		
       	JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		
		JLabel head = new JLabel("** Welcome To The Game **");
		panel1.add(head);

		JLabel name = new JLabel("Name: ");
		panel2.add(name);

		textField = new JTextField(10);
		panel2.add(textField);

		JCheckBox chk1 = new JCheckBox("Warrior");
		JCheckBox chk2 = new JCheckBox("Wizard");

		chk1.addActionListener ( new ActionListener ( ) 
			{		
				public void actionPerformed ( ActionEvent event ) 
				{
					JCheckBox chk1 = ( JCheckBox ) event.getSource ( );

					boolean isSelected = chk1.isSelected ( );
		
					if (isSelected)
					{
						Control lct = new Control(textField.getText(),"Warrior");
						lct.setVisible(true);
					}
				}
			}
		);

		chk2.addActionListener ( new ActionListener ( ) 
			{		
				public void actionPerformed ( ActionEvent event ) 
				{
					JCheckBox chk2 = ( JCheckBox ) event.getSource ( );

					boolean isSelected = chk2.isSelected ( );
		
					if (isSelected)
					{
						Control lct = new Control(textField.getText(),"Wizard");
						lct.setVisible(true);
					}
				}
			}
		);
		panel.add(panel1);
		panel.add(panel2);
		panel.add(chk1);
		panel.add(chk2);
		
		f.add(panel);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(300, 200);
		f.setResizable(false);
		f.setLocationRelativeTo(null); 

	}
}
	