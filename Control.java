import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import javax.swing.JLayeredPane;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;




public class Control extends JFrame
{
    JFrame f;
    JPanel pl, pl_B, pl_HP, pl_EXP, pl_name, pl_level, pl_Kill,pl_Name,pl_mon, career, Over;
    JLabel hp, exp, Kill_M, LEVEL, Name, kill, levelN, mon, gameOverLabel, Career_Text,career_Novice,over;
    JProgressBar progressBar_HP, progressBar_EXP;
    JLabel background, imgNovice, img_Novice;
    String job;
    int monster = 0;
    int  Exp = 0;
    int level = 1;
    int Hp = 100;

    public Control(String name ,String job)
    {
        Image(name);
        Career_LeveL(name, job);
        Novice(job);
        
    }
    public void Image(String name)
    {
        ImageIcon img = new ImageIcon("Galaxy.jpg");
        background = new JLabel(img);
        background.setBounds(0,0,960,600);
        setContentPane(background);
        
        ImageIcon img_nv = new ImageIcon("Novice.gif");
        imgNovice = new JLabel("", img_nv, JLabel.CENTER);
        imgNovice.setBounds(2,5,70,80);
        ProgressBar();
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900,600);
        setLocationRelativeTo(null); 
        setResizable(false);
        
        Button(name);

        add(imgNovice);
    }

    public void Novice(String job)
    {
        if(job.equals("Wizard"))
        {
            ImageIcon novice = new ImageIcon("Wizard.gif");
            img_Novice = new JLabel("", novice, JLabel.CENTER);
            img_Novice.setBounds(130,20,700,700);
            add(img_Novice);
        }
        else if(job.equals("Warrior"))
        {
            ImageIcon novice = new ImageIcon("Warrior.gif");
            img_Novice = new JLabel("", novice, JLabel.CENTER);
            img_Novice.setBounds(200,2,400,500);
            add(img_Novice);
        }
    }

    public void ProgressBar()
    {
        hp = new JLabel("HP  ");
        exp =new JLabel("EXP");
        pl = new JPanel();

        pl_HP = new JPanel();
        pl_HP.setLayout(new BorderLayout());
        progressBar_HP = new JProgressBar();
        progressBar_HP.setBounds(0, 0, 196, 5);
        progressBar_HP.setStringPainted(true);
        progressBar_HP.setForeground(Color.red);
        progressBar_HP.setValue(100);
        pl_HP.add(hp, BorderLayout.WEST);
        pl_HP.add(progressBar_HP,BorderLayout.CENTER);
        pl_HP.setBorder(new BevelBorder(BevelBorder.RAISED));

        pl_EXP = new JPanel();
        pl_EXP.setLayout(new BorderLayout());
        progressBar_EXP = new JProgressBar();
        progressBar_EXP.setBounds(0, 0, 196, 5);
        progressBar_EXP.setStringPainted(true);
        progressBar_EXP.setForeground(Color.BLUE);
        progressBar_EXP.setValue(0);
        pl_EXP.add(exp, BorderLayout.WEST);
        pl_EXP.add(progressBar_EXP);
        pl_EXP.setBorder(new BevelBorder(BevelBorder.RAISED));
        pl.setLayout(new BoxLayout(pl, BoxLayout.Y_AXIS));
        pl.setBounds(90,30,200,50);
        pl.add(pl_HP);
        pl.add(pl_EXP);
        add(pl);   
    }

    public void Button(String name)
    {
        gameOverLabel = new JLabel();
        pl_B = new JPanel();
        pl_B.setBounds(330,407,220,150);

        pl_B.setLayout( new BorderLayout());

        JButton b1_T = new JButton("TOP");
        pl_B.add(b1_T,  BorderLayout.NORTH);
        JButton b2_D = new JButton("DOWN");
        pl_B.add(b2_D, BorderLayout.SOUTH);
        JButton b3_R = new JButton("RIGHT");
        pl_B.add(b3_R, BorderLayout.EAST);
        JButton b4_L = new JButton("LEFT");
        pl_B.add(b4_L ,BorderLayout.WEST);
         JButton b5_A = new JButton("ATTACK");
        pl_B.add(b5_A, BorderLayout.CENTER);

        b1_T.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			   Object source = e.getSource();
			    if(source == b1_T){
                    Hp -= 20;
                    Exp += 20;
                    monster += 2;
                    String mst = Integer.toString(monster);
                    kill.setText(mst);
                    if(Hp >= 0){
                        JOptionPane.showMessageDialog(null, "Game Over");  
                        dispose();
                    } 
                    if(Exp >= 100)
                    {
                        Exp = Exp-100;
                        Hp = 100;
                        level++;
                    }   
                    String lvl = Integer.toString(level);
                    levelN.setText(lvl);
                    progressBar_HP.setValue(Hp);
                    progressBar_EXP.setValue(Exp);
                    mon.setText(mst);
			   }
			}
        });       

        b2_D.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			   Object source = e.getSource();
			    if(source == b2_D){
                    Hp -= 5;
                    Exp += 15;
                    progressBar_HP.setValue(Hp);
                    progressBar_EXP.setValue(Exp);
                    String mst = Integer.toString(monster);
                    kill.setText(mst); 
                    if(Hp <= 0){
                        JOptionPane.showMessageDialog(null, "Game Over");  
                        dispose();
                    } 
                    if(Exp >= 100)
                    {
                        Exp = Exp-100;
                        Hp = 100;
                        level++;
                    }   
                    String lvl = Integer.toString(level);
                    levelN.setText(lvl);
                    mon.setText(mst);
			   }
			}
        });


        b3_R.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			   Object source = e.getSource();
			    if(source == b3_R){
                    Hp -= 2;
                    Exp += 10;
                    progressBar_HP.setValue(Hp);
                    progressBar_EXP.setValue(Exp);
                    monster += 3;
                    String mst = Integer.toString(monster);
                    kill.setText(mst); 
                    if(Hp <= 0){
                        JOptionPane.showMessageDialog(null, "Game Over");  
                        dispose();
                    } 
                    if(Exp >= 100)
                    {
                        Exp = Exp-100;
                        Hp = 100;
                        level++;
                    }   
                    String lvl = Integer.toString(level);
                    levelN.setText(lvl);
                    mon.setText(mst);
			   }
			}
        });

        b4_L.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			   Object source = e.getSource();
			    if(source == b4_L){
                    Hp -= 35;
                    Exp += 35;
                    progressBar_HP.setValue(Hp);
                    progressBar_EXP.setValue(Exp);
                    monster += 1;
                    String mst = Integer.toString(monster);
                    kill.setText(mst); 
                    if(Hp <= 0){
                        JOptionPane.showMessageDialog(null, "Game Over");  
                        dispose();
                    } 
                    if(Exp >= 100)
                    {
                        Exp = Exp-100;
                        Hp = 100;
                        level++;
                    }   
                    String lvl = Integer.toString(level);
                    levelN.setText(lvl);
                    mon.setText(mst);
			   }
			}
        });

        b5_A.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
			   Object source = e.getSource();
                    Hp -= 20;
                    Exp += 20;
                    monster += 5;
                    progressBar_HP.setValue(Hp);
                    progressBar_EXP.setValue(Exp);
                    String mst = Integer.toString(monster);
                    kill.setText(mst); 
                    if(Hp <= 0){
                       JOptionPane.showMessageDialog(null, "Game Over");           
                       dispose();      
                    } 
                    if(Exp >= 100)
                    {
                        Exp = Exp-100;
                        Hp = 100;
                        level++;
                    }   
                    String lvl = Integer.toString(level);
                    levelN.setText(lvl);
                    mon.setText(mst);
                    }
             
        });

        add(pl_B);
    }
    
    public void Career_LeveL(String name ,String job)
    {   
        Name = new JLabel(name);
        LEVEL =  new JLabel("Leve: ");
        Kill_M = new JLabel("Kill_M: ");
        kill = new JLabel();
        mon = new JLabel();
        levelN = new JLabel();
        Career_Text = new JLabel("Career: ");
        career_Novice = new JLabel(job);
        
        Name.setHorizontalAlignment (SwingConstants.CENTER);
        pl_Name = new JPanel();
        pl_Name.setLayout(new BorderLayout());
        pl_Name.add(Name, BorderLayout.CENTER);
        

        pl_level = new JPanel();
        pl_level.setLayout(new BorderLayout());
        pl_level.add(LEVEL, BorderLayout.WEST);
        pl_level.add(levelN, BorderLayout.CENTER);
        

        pl_Kill = new JPanel();
        pl_Kill.setLayout(new BorderLayout());
        pl_Kill.add(Kill_M, BorderLayout.WEST);
        pl_Kill.add(kill, BorderLayout.CENTER);

        career = new JPanel();
        career.setLayout(new BorderLayout());
        career.add(Career_Text, BorderLayout.WEST);
        career.add(career_Novice, BorderLayout.CENTER);

        pl_mon = new JPanel();
        pl_mon.setLayout(new BorderLayout());
        pl_mon.add(new JLabel("Monster:"), BorderLayout.WEST);
        pl_mon.add(mon, BorderLayout.CENTER);
        
        pl_name = new JPanel();
        pl_name.setLayout(new BoxLayout(pl_name, BoxLayout.Y_AXIS));
        pl_name.setBounds(8,90,120,80);
        pl_name.setBorder(new BevelBorder(BevelBorder.RAISED));
        pl_name.add(pl_Name);
        pl_name.add(pl_level);
        pl_name.add(pl_Kill);
        pl_name.add(pl_mon);
        pl_name.add(career);
        add(pl_name);
        add(gameOverLabel);
    }
}